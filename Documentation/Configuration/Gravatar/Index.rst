.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../../Includes.txt

.. _gravatar:

Gravatar
========

===================================== ========
Property                               Type
===================================== ========
showGravatarImage_                     boolean
gravatarSize_                          integer
gravatarDefault_                       string
===================================== ========

.. _showGravatarImage:

showGravatarImage
"""""""""""""""""
.. container:: table-row

   Property
      showGravatarImage
   Data type
      boolean
   Default
      1
   Description
      Enables or disables the usage of gravatar images

.. _gravatarSize:

gravatarSize
""""""""""""
.. container:: table-row

   Property
      gravatarSize
   Data type
      integer
   Default
      50
   Description
      Size (in pixel) of gravatar images

.. _gravatarDefault:

gravatarDefault
"""""""""""""""
.. container:: table-row

   Property
      gravatarDefault
   Data type
      string
   Default
      mm
   Description
      This specifies the default image for e-mail addresses without a gravatar
